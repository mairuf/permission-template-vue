import { createApp } from "vue";
import App from "./App.vue";

import router from "@/router/index";

import "element-plus/dist/index.css";
import "@/styles/index.scss";
import { useElementComponents } from "@/config/element";

import { createPinia } from "pinia";

const app = createApp(App);

app.use(createPinia())
app.use(router);
app.use(useElementComponents);

app.mount("#app");
