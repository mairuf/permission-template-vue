import request from "@/utils/request";

export default {
    /**
     * 获取所有菜单列表
     */
    list() {
        return request({
            url: "/menu",
            method: "get",
        })
    }
    ,
    /**
     * 根据 用户信息 获取 该用户 所可以访问的 菜单项
     */
    listByUser(param: any) {
        return request({
            url: "/menu",
            method: "get",
            data: param
        })
    }
}
