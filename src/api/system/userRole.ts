import request from "@/utils/request";

export default {
    list() {
        return request({
            url: "/user/role",
            method: "get",
        })
    },
    page(param: any) {
        return request({
            url: "/user/role",
            method: "get",
            data: param
        })
    },
    getListByUserId(param: any){
        return request({
            url: "/user/role/userId/" + param.userId,
            method: "get",
        })
    },
    addUserRole(param: any) {
        return request({
            url: "/user/role",
            method: "post",
            data: param
        })
    },
    updateUserRole(param: any) {
        return request({
            url: "/user/role",
            method: "put",
            data: param
        })
    },
    deleteUserRole(param: any) {
        return request({
            url: "/user/role/",
            method: "delete",
            data: param
        })
    }

}
