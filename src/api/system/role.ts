import request from "@/utils/request";

export default {
    list() {
        return request({
            url: "/role",
            method: "get",

        })
    },
    addRole(param: any) {
        return request({
            url: "/role",
            method: "post",
            data: param
        })
    },
    updateRole(param: any) {
        return request({
            url: "/role",
            method: "put",
            data: param
        })
    },
    deleteRole(param: any) {
        return request({
            url: "/role/" + param,
            method: "delete",
        })
    }
}
