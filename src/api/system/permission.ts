import request from "@/utils/request";

export default {
    list() {
        return request({
            url: "/permission",
            method: "get",

        })
    },
    addScanPermission() {
        return request({
            url: "/permission/addScanPermission",
            method: "get",
        })
    },
    getByRoleId(param: any){
        return request({
            url: "/permission/getByRole/" + param.roleId + "/" + param.type,
            method: "get",
        })
    }
}
