import request from "@/utils/request";

export default {
    list() {
        return request({
            url: "/user",
            method: "get",
        })
    },
    page(param: any) {
        return request({
            url: "/user",
            method: "get",
            data: param
        })
    },
    addUser(param: any) {
        return request({
            url: "/user",
            method: "post",
            data: param
        })
    },
    updateUser(param: any) {
        return request({
            url: "/user",
            method: "put",
            data: param
        })
    },
    deleteUser(param: any) {
        return request({
            url: "/user/" + param,
            method: "delete",
        })
    }

}
