import request from "@/utils/request";

export default {
    list() {
        return request({
            url: "/role/permission",
            method: "get",
        })
    },
    page(param: any) {
        return request({
            url: "/role/permission",
            method: "get",
            data: param
        })
    },
    getListByRoleId(param: any){
        return request({
            url: "/role/permission/roleId/" + param.roleId,
            method: "get",
        })
    },
    addBatchRolePermission(param: any) {
        return request({
            url: "/role/permission/saveBatch",
            method: "post",
            data: param
        })
    },
    updateRolePermission(param: any) {
        return request({
            url: "/role/permission",
            method: "put",
            data: param
        })
    },
    deleteBatchRolePermission(param: any) {
        return request({
            url: "/role/permission/deleteBatch",
            method: "delete",
            data: param
        })
    }

}
