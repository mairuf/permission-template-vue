import request from "@/utils/request";

export default {
    login(param: any) {
        return request({
            url: "/authentication/login",
            method: "post",
            data: param
        })
    },
    logout() {
        return request({
            url: "/authentication/logout",
            method: "get",
        })
    }
}
