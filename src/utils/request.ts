import axios from "axios";
import {ElMessageBox} from "element-plus";
import {useUserStore} from "@/stores/user";
import router from "@/router";

// axios 默认配置：
// 超时时间
axios.defaults.timeout = 10000;
// 请求地址前缀 - 开发
axios.defaults.baseURL = 'http://localhost:8008/api/v1'

// 请求拦截
axios.interceptors.request.use(
    config => {
        // @ts-ignore
        config.headers['Content-Type'] = 'application/json;charset=UTF-8';
        const token = sessionStorage.getItem("token");
        if (token) {
            // @ts-ignore
            config.headers['token'] = token;
        }
        return config;
    },

    error => {
        return Promise.reject(error.response);
    }
)

// 响应拦截
axios.interceptors.response.use(
    response => {
        const userStore = useUserStore();
        /**
         * 登录拦截
         *  当登录失效 或 用户未登录时，跳转至登录页
         */
        if (response.data.status === false && response.data.message === "请先登录") {

            ElMessageBox.alert('您好  ' + userStore.name + ' 当前登录已失效，请重新登录', '登录失效', {
                confirmButtonText: '去登录',
                callback: () => {
                    // 移除 token
                    sessionStorage.removeItem('token')
                    sessionStorage.removeItem('username')
                    router.push('/login')
                }
            })
        } else if (response.data.status === false && response.data.message === "您的权限不足") {
            ElMessageBox.alert('抱歉！ ' + userStore.name + ' 您暂无权限进行该操作', '权限不足', {
                confirmButtonText: '好的',
                callback: () => {
                    router.push('/')
                }
            })
        } else {
            return response.data;
        }
    },

    error => {
        // 返回接口返回的错误信息
        return Promise.reject(error.response)
    }
)

export default axios;
