/**
 * 本文件为 element-plus 组件导入配置
 * 主要作用就是 按需 导入组件
 */
import {App} from 'vue'
import {
    ElContainer,
    ElHeader,
    ElMain,
    ElInput,
    ElButton,
    ElMenu,
    ElSubMenu,
    ElMenuItem,
    ElIcon,
    ElAvatar,
    ElDropdown,
    ElDropdownItem,
    ElDropdownMenu,
    ElTable,
    ElTableColumn,
    ElDialog,
    ElMessageBox,
    ElForm,
    ElInputNumber,
    ElPopconfirm,
    ElPopper,
    ElSwitch,
    ElRadio,
    ElRadioButton,
    ElRadioGroup,

} from 'element-plus'

const components = [
    ElContainer,
    ElHeader,
    ElMain,
    ElInput,
    ElButton,
    ElMenu,
    ElSubMenu,
    ElMenuItem,
    ElIcon,
    ElAvatar,
    ElDropdown,
    ElDropdownMenu,
    ElDropdownItem,
    ElTable,
    ElTableColumn,
    ElDialog,
    ElMessageBox,
    ElForm,
    ElInputNumber,
    ElPopconfirm,
    ElPopper,
    ElSwitch,
    ElRadio,
    ElRadioButton,
    ElRadioGroup,

]

import {CaretBottom, Expand, Fold} from '@element-plus/icons'

const icons = [Fold, Expand, CaretBottom]

export function useElementComponents(app: App): void {
    components.forEach((component) => {
        app.component(component.name, component)
    })

    icons.forEach((icon) => {
        app.component(icon.name, icon)
    })
}
