import {markRaw} from "vue";
import LayoutIndex from "@/components/layout/index.vue";

/**
 * 静态路由，与权限无关，所有用户都可以访问
 * hidden: true，不显示在菜单中
 * alwaysShow: true，默认当子路由只有一个时，只显示子菜单，当 alwaysShow 设为 true时，永远显示该菜单
 * LayoutIndex 不是按需引入的，加上 markRaw 可以解决 vue 报性能警告
 * meta.breadcrumb = false 表示该菜单的标题不显示在面包屑中
 */
// 商家管理相关路由
export const systemRoutes = [
    {
        path: '/system',
        component: markRaw(LayoutIndex),
        meta: {title: '系统管理'},
        // redirect: '/authorization',
        children: [
            {
                path: 'user',
                name: 'User',
                component: () => import('@/views/system/user/User.vue'),
                meta: {title: '用户管理'}
            },
            {
                path: 'user/grantRole',
                name: 'UserGrantRole',
                component: () => import('@/views/system/user/grantRole/index.vue'),
                meta: {title: '用户角色管理'}
            },
            {
                path: 'role',
                name: 'Role',
                component: () => import('@/views/system/role/Role.vue'),
                meta: {title: '角色管理'}
            },
            {
                path: 'permission',
                name: 'Permission',
                component: () => import('@/views/system/permission/Permission.vue'),
                meta: {title: '权限管理'}
            },
            {
                path: 'role/grantPermission',
                name: 'RoleGrantPermission',
                component: () => import('@/views/system/role/grantPermission/index.vue'),
                meta: {title: '角色权限管理'}
            },
        ]
    }
];
