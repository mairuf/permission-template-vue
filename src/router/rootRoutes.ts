import {markRaw} from "vue";
import LayoutIndex from "@/components/layout/index.vue";


/**
 * 静态路由，与权限无关，所有用户都可以访问
 * hidden: true，不显示在菜单中
 * alwaysShow: true，默认当子路由只有一个时，只显示子菜单，当 alwayShow 设为 true时，永远显示该菜单
 * LayoutIndex 不是按需引入的，加上 markRaw 可以解决 vue 报性能警告
 * meta.breadcrumb = false 表示该菜单的标题不显示在面包屑中
 */
// 根目录相关路由：如登录界面、首页等处于 views 根目录下的组件
export const rootRoutes = [
    {
        path: '/',
        component: markRaw(LayoutIndex),
        redirect: '/dashboard',
        children: [
            {
                path: 'dashboard',
                component: () => import('@/views/dashboard/Dashboard.vue'),
                meta: {title: '首页'}
            }
        ]
    },
    {
        path: '/:pathMatch(.*)*',
        redirect: '404',
        hidden: true
    }
]
