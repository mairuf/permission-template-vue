/**
 * 静态路由，与权限无关，所有用户都可以访问
 * hidden: true，不显示在菜单中
 * alwaysShow: true，默认当子路由只有一个时，只显示子菜单，当 alwayShow 设为 true时，永远显示该菜单
 * LayoutIndex 不是按需引入的，加上 markRaw 可以解决 vue 报性能警告
 * meta.breadcrumb = false 表示该菜单的标题不显示在面包屑中
 */
// 错误页面路由
export const errorRoutes = [
    {
        path: '/403',
        component: () => import('@/views/error/403Page.vue'),
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/error/404Page.vue'),
        hidden: true
    }
];
