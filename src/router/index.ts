import {createRouter, createWebHistory} from 'vue-router'

/**
 * 静态路由，与权限无关，所有用户都可以访问
 * hidden: true，不显示在菜单中
 * alwaysShow: true，默认当子路由只有一个时，只显示子菜单，当 alwayShow 设为 true时，永远显示该菜单
 * LayoutIndex 不是按需引入的，加上 markRaw 可以解决 vue 报性能警告
 * meta.breadcrumb = false 表示该菜单的标题不显示在面包屑中
 */
import {rootRoutes} from "@/router/rootRoutes";
import {authRoutes} from "@/router/authRoutes";
import {systemRoutes} from "@/router/systemRoutes";
import {errorRoutes} from "@/router/errorRoutes";


// 拼接路由
const baseRoutes: any[] = [];
export const routes = baseRoutes.concat(rootRoutes, authRoutes, systemRoutes, errorRoutes);
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: routes
})

/**
 * 每次切换路由前执行
 * 如果没有 token，跳转到登录页
 * 如果有 token，进一步判断是否有角色，有角色就进入路由
 * 如果没有角色就获取角色，然后根据获取到的角色过滤动态路由
 * 最终将过滤后的动态路由添加到路由表中
 */
router.beforeEach((to) => {
    const token = sessionStorage.getItem('token')
    if (token) {
        if (to.path === '/login') {
            return '/'
        } else {
        }
    } else {
        if (to.path !== '/login') {
            return '/login'
        }
    }
})

export default router
