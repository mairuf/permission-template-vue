export const authRoutes = [
    {
        path: '/login',
        component: () => import('@/views/auth/Login.vue'),
        hidden: true
    },
];
