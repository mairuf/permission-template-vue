import {defineStore} from 'pinia';
import {ElMessageBox} from 'element-plus'
import auth from "@/api/auth";
import router from '@/router/index'

export const useUserStore = defineStore('user', {
    state: () => ({
        token: sessionStorage.getItem('token'),
        name: sessionStorage.getItem('username'),
        roles: [],
        menus: []
    }),
    actions: {
        // 登录成功后将返回的 用户信息 存起来
        login(param: any) {
            return auth.login(param).then((res: any) => {
                    if (res.status) {
                        this.token = res.data.token
                        this.name = res.data.username
                        // 将 token 和 用户名存入 sessionStorage ，以便在本系统中使用
                        sessionStorage.setItem('token', res.data.token)
                        sessionStorage.setItem('username', res.data.username)
                        // TODO 获取用户可以访问的菜单，用于后续渲染菜单项
                        /*
                        menu.list().then(res => {
                            if (res.status) {
                                this.menus = res.data
                            }
                            console.log(this.menus)
                        })
                        */
                        ElMessageBox.alert('您好 ' + this.name + ' ，欢迎访问本系统', '登录成功', {
                            confirmButtonText: '你好',
                        })
                    }
                }
            )

        },

        // 清空 用户信息 并跳转到登录页
        logout() {
            return auth.logout().then((res: any) => {
                if (res.status) {

                    this.token = ''
                    this.roles = []
                    this.menus = []
                    sessionStorage.removeItem('token')
                    sessionStorage.removeItem('username')
                    ElMessageBox.alert('再见 ' + this.name + ' ，欢迎再次访问本系统', '退出登录成功', {
                        confirmButtonText: '再见',
                    })
                    router.push('/login')
                }
            })
        }
    }
})
